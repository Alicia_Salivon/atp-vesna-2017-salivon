﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            int i, j;
            double det;
            double[,] mas1 = new double[3, 3];
            for (i = 0; i < 3; i++) 
            {
                for (j = 0; j < 3; j++) 
                {
                    mas1[i,j] = Convert.ToDouble(Console.ReadLine());
                }
            }
            double[,] mas2 = new double[3, 3];
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 3; j++)
                {
                    mas2[i, j] = Convert.ToDouble(Console.ReadLine());
                }
            }
            det = mas2[0, 0] * mas2[1, 1] * mas2[2, 2] + mas2[0, 1] * mas2[1, 2] * mas2[2, 0] + mas2[1, 0] * mas2[2, 1] * mas2[0, 2] - mas2[0, 2] * mas2[1, 1] * mas2[2, 0] - mas2[0, 0] * mas2[1, 2] * mas2[2, 1] - mas2[0, 1] * mas2[1, 0] * mas2[2, 2];
            double [,] mas21 = new double [3, 3];
            mas21[0,0] = Math.Pow(-1, 2) * mas2[1, 1] * mas2[2, 2] - mas2[1, 2] * mas2[2, 1];
            mas21[0,1] = Math.Pow(-1, 3) * mas2[0, 1] * mas2[2, 2] - mas2[0, 2] * mas2[2, 1];
            mas21[0,2] = Math.Pow(-1, 4) * mas2[0, 1] * mas2[1, 2] - mas2[0, 2] * mas2[1, 1];
            mas21[1,0] = Math.Pow(-1, 3) * mas2[1, 0] * mas2[2, 2] - mas2[1, 2] * mas2[2, 2];
            mas21[1,1] = Math.Pow(-1, 4) * mas2[0, 0] * mas2[2, 2] - mas2[0, 2] * mas2[2, 0];
            mas21[1,2] = Math.Pow(-1, 5) * mas2[0, 0] * mas2[1, 2] - mas2[0, 2] * mas2[1, 0];
            mas21[2,0] = Math.Pow(-1, 4) * mas2[1, 0] * mas2[2, 1] - mas2[1, 1] * mas2[2, 0];
            mas21[2,1] = Math.Pow(-1, 5) * mas2[0, 0] * mas2[2, 1] - mas2[0, 1] * mas2[2, 0];
            mas21[2,2] = Math.Pow(-1, 6) * mas2[0, 0] * mas2[1, 1] - mas2[0, 1] * mas2[1, 0];
            double [,] mas211 = new double [3, 3];
            for (i = 0; i < 3; i++) 
            {
                for (j = 0; j < 3; j++) 
                {
                    mas211[i, j] = (1 / det) * mas21[i, j];
                }
            }
            double[,] mas3 = new double[3, 3];
            for (i = 0; i < 3; i++) 
            {
                for (j = 0; j < 3; j++) 
                {
                    for (int n = 0; n < 3; n++)
                    {
                        mas3[i, j] = mas1[i, n] * mas211[n, j];
                    }
                }

            }
            for (i = 0; i < 3; i++) 
            {
                for (j = 0; j < 3; j++)
                {
                    Console.WriteLine("{0}", mas3[i, j]);
                    Console.WriteLine();
                }
            }
            Console.ReadKey();
        }
    }
}
