﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            int z, j, i, tmp;
            int[,] A = new int[4, 4];
            int[,] C = new int[4, 4];
            int[] B = new int[4];
            Console.WriteLine("Input mass A:");
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    A[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }
            Console.WriteLine("\nInput mass B:");
            for (i = 0; i < 4; i++)
            {
                B[i] = Convert.ToInt32(Console.ReadLine());
            }
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    C[i, j] = A[i, j];
                }
            }
            for (i = 0; i < 4; i++)
            {
                C[2, i] = B[i];
            }
            Console.WriteLine("\nC:");
            for (i = 0; i < 4; i++)
            {
                Console.WriteLine();
                for (j = 0; j < 4; j++)
                {
                     Console.Write(C[i,j] + " ");
                }
            }
            for (j = 0; j < 4 - 1; ++j)
            {
                for (z = j + 1; z < 4; ++z)
                {
                    if (C[2, j] > C[2, z])
                    {
                        tmp = C[2, j];
                        C[2, j] = C[2, z];
                        C[2, z] = tmp;
                    }
                }
            }
            Console.WriteLine("\nMax el: " + C[2, 3]);
            string str1 = "aaa BBb cCc";
            string str2 = "Test1 test2 test2.";
            string[] str3 = new string[2];
            str3[0] = str1;
            str3[1] = str2;
            Console.WriteLine("\n" + str3[0] + " " + str3[1]);
            Console.ReadKey();
        }
    }
}
