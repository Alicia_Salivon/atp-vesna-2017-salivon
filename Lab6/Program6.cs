﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        public struct student
        {
            public string name;
            public string pred;
            public int math;
            public int bal;
            public int school;
            public string clas;
        }
        static void Main(string[] args)
        {
            int i;
            student[] A = new student[4];
            for (i = 0; i < 4; i++)
            {
                Console.Write("Введите имя студента: ");
                A[i].name = Console.ReadLine();
                Console.Write("Введите любимий предмет студента: ");
                A[i].pred = Console.ReadLine();
                Console.Write("Введите бал по математике студента: ");
                A[i].math = Convert.ToInt32(Console.ReadLine());
                Console.Write("Введите средний бал студента: ");
                A[i].bal = Convert.ToInt32(Console.ReadLine());
                Console.Write("Введите школу студента: ");
                A[i].school = Convert.ToInt32(Console.ReadLine());
                Console.Write("Введите клас студента: ");
                A[i].clas = Console.ReadLine();
                Console.WriteLine();
            }
            for (i = 1; i < 4; i++)
            {
                if (A[i - 1].bal > A[i].bal)
                {
                    A[i].bal = A[i - 1].bal;
                }
                else A[i - 1].bal = A[i].bal;
            }
            Console.WriteLine("Наивысший бал: " + A[3].bal);
            Console.ReadKey();
        }
    }
}
