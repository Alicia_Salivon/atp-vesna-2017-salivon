﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication28
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            Queue<int> q = new Queue<int>();   
            string exit = "";
            int u;
            u = rand.Next(3, 5);
            bool[] used = new bool[u + 1];  
            int[][] g = new int[u + 1][];   
            for (int i = 0; i < u + 1; i++)
            {
                g[i] = new int[u + 1];
                Console.Write("\n({0}) vertex -->[", i + 1);
                for (int j = 0; j < u + 1; j++)
                {
                    g[i][j] = { };
                }
                g[i][i] = 0;
                foreach (var item in g[i])
                {
                    Console.Write(" {0}", item);
                }
                Console.Write("]\n");
            }
            used[u] = true;    

            q.Enqueue(u);
            Console.WriteLine("Begin traversal from {0} vertices", u + 1);
            while (q.Count != 0)
            {
                u = q.Peek();
                q.Dequeue();
                Console.WriteLine("Passed to the node {0}", u + 1);

                for (int i = 0; i < g.Length; i++)
                {
                    if (Convert.ToBoolean(g[u][i]))
                    {
                        if (!used[i])
                        {
                            used[i] = true;
                            q.Enqueue(i);
                            Console.WriteLine("Added node to queue {0}", i + 1);
                        }
                    }
                }
            }
            Console.ReadKey();
        }
    }
}
