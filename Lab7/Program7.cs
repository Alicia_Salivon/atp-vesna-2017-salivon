﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            double b, c, x, a, h;
            Console.Write("x = ");
            x = Convert.ToDouble(Console.ReadLine());
            a = A(x);
            b = B(x);
            c = C(x);
            h = H(c, b);
            Console.Write("\nA = " + a);
            Console.Write("\nB = " + b);
            Console.Write("\nC = " + c);
            Console.Write("\nH = " + h);
            Console.ReadKey();
        }

        static public double H(double c, double b)
        {
            double y;
            y = 6 * Math.Pow(b, 3) + 4 * c - 2;
            return y;
        }

        static public double A(double x)
        {
            double y;
            y = Math.Tan(x) + Math.Pow(Math.E, 2 * x);
            return y;
        }

        static public double B(double x)
        {
            double y;
            y = x * x - 6 * x * x * x;
            return y;
        }

        static public double C(double x)
        {
            double y;
            y = (1 / x) - 2 * Math.Log(x);
            return y;
        }
    }
}
