﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            double f1, f2;
            double x = Convert.ToDouble(Console.ReadLine());
            double a = 2.0, b = 6.3, c = 0.4;
            f1 = (x * Math.Sin(x * x * x)) - (Math.Log10(2 * x));
            f2 = Math.Atan(x) / 4 + Math.Pow(Math.E, -x) + 2;
            while (a < b)
            {
                f1 += (x * Math.Sin(x * x * x)) - (Math.Log10(2 * x));
                f2 += Math.Atan(x) / 4 + Math.Pow(Math.E, -x) + 2;
                a += c;
            }
            Console.WriteLine("\nf1(x) = {0}\nf2(x) = {1}", f1, f2);
            Console.ReadKey();
        }
    }
}
