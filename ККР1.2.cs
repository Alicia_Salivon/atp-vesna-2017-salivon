﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            int i, j, sum = 0;
            int[,] mas = new int[5, 5];
            Console.WriteLine("Mass");
            for(i = 0; i < 5; i++)
            {
                for(j = 0; j < 5; j++)
                {
                    mas[i, j] = i + j;
                }
            }
            for (i = 0; i < 5; i++)
            {
                for (j = 0; j < 5; j++)
                {
                    Console.Write(mas[i, j] + " ");
                }
                Console.WriteLine();
            }
            for (i = 0; i < 5; i++)
            {
                for (j = 0; j < 5; j++)
                {
                    if(mas[i,j]%2 > 0)
                    {
                        sum += mas[i, j];
                    }
                }
            }
            Console.WriteLine("Сума = " + sum);
            Console.ReadKey();
        }
    }
}
